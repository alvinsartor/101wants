package com.ananasproject.hundredwants;

import android.app.Notification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by Alvin on 10/01/2017.
 */

public class Receiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        String title = context.getString(R.string.notificationTitle);
        String text = context.getString(R.string.notificationText);
        sharedFunctions.createNotification(context, title, text);

        //sharedFunctions.setNotificationTomorrowAtThisHour(context);
    }
}
