package com.ananasproject.hundredwants;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.ananasproject.hundredwants.data.WantsContract;
import com.ananasproject.hundredwants.dragndroplist.*;

import static android.R.attr.data;
import static com.ananasproject.hundredwants.sharedFunctions.*;

/**
 * A placeholder fragment containing a simple view.
 */
public class ActiveWantsActivityFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    static TextView nextNumber;
    ActiveWantsCursorAdapter mCursorAdapter;

    View emptyListItem;
    EditText textOfNextWant;
    public ActiveWantsActivityFragment() { }

    @Override
    public void onStart() {
        super.onStart();
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        textOfNextWant = (EditText) emptyListItem.findViewById(R.id.futureWantText);
        if (sharedPref.getBoolean("addPrefix_key", false))
            textOfNextWant.setText(getString(R.string.iwant_prefix) + " ");
        else
            textOfNextWant.setText("");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());

        // Find the ListView which will be populated with the pet data
        //ListView wantsListView = (ListView) rootView.findViewById(R.id.list);
        DragNDropListView wantsListView = (DragNDropListView) rootView.findViewById(R.id.list);
        // Finding the empty-item and saving its components
        emptyListItem = View.inflate(getContext(), R.layout.list_item_empty, null);
        nextNumber = (TextView) emptyListItem.findViewById(R.id.futureNumber);
        nextNumber.setText("#" + getNextWantNumber(getContext()));

        textOfNextWant = (EditText) emptyListItem.findViewById(R.id.futureWantText);
        if (sharedPref.getBoolean("addPrefix_key", true))
            textOfNextWant.setText(getString(R.string.iwant_prefix) + " ");
        else
            textOfNextWant.setText("");

        // adding listener to the ADD button
        emptyListItem.findViewById(R.id.btAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textOfNextWant.getText().toString().trim().length() > 0 &
                        !textOfNextWant.getText().toString().trim().equals(getString(R.string.iwant_prefix))
                        ) {
                    // getting number and text and inserting want into DB
                    int nr = Integer.parseInt(nextNumber.getText().toString().substring(1));
                    insertActiveWant(getContext(), nr, textOfNextWant.getText().toString());
                    if (sharedPref.getBoolean("addPrefix_key", true))
                        textOfNextWant.setText(getString(R.string.iwant_prefix) + " ");
                    else
                        textOfNextWant.setText("");
                    // actualize number in next want
                    nextNumber.setText("#" + getNextWantNumber(getContext()));
                    // hide keyboard
                    InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
                }
                else
                    Snackbar.make(view, getString(R.string.write_something), Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });
        wantsListView.addFooterView(emptyListItem);


        // Find and set empty view on the ListView, so that it only shows when the list has 0 items.
        //View emptyView = rootView.findViewById(R.id.empty_view);
        //wantsListView.setEmptyView(emptyView);


        // Setup an Adapter to create a list item for each row of pet data in the Cursor.
        // There is no data yet (until the loader finishes) so pass in null for the Cursor.
        mCursorAdapter = new ActiveWantsCursorAdapter(getContext(), null, this, R.id.number);
        //wantsListView.setAdapter(mCursorAdapter);
        wantsListView.setDragNDropAdapter(mCursorAdapter);

        // Kick off the loader
        getLoaderManager().initLoader(0, null, this);
        return rootView;
    }

    public Loader<Cursor> reload() {
        // Define a projection that specifies the columns from the table we care about.
        String[] projection = {
                WantsContract.ActiveEntry._ID,
                WantsContract.ActiveEntry.COLUMN_NR,
                WantsContract.ActiveEntry.COLUMN_TEXT,
                WantsContract.ActiveEntry.COLUMN_INSERT_DATE,
                WantsContract.ActiveEntry.COLUMN_CATEGORY
        };

        // This loader will execute the ContentProvider's query method on a background thread
        return new CursorLoader(getContext(),            // Parent activity context
                WantsContract.ActiveEntry.CONTENT_URI,   // Provider content URI to query
                projection,                              // Columns to include in the resulting Cursor
                null,                                    // No selection clause
                null,                                    // No selection arguments
                WantsContract.ActiveEntry.COLUMN_NR + " ASC");     // Default sort order
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) { return reload(); }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // Update {@link PetCursorAdapter} with this new cursor containing updated pet data
        mCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // Callback called when the data needs to be deleted
        mCursorAdapter.swapCursor(null);
    }


}
