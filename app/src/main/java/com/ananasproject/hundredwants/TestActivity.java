package com.ananasproject.hundredwants;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.view.View;
import android.widget.Button;

import java.util.Calendar;

/**
 * Created by Alvin on 10/01/2017.
 */

public class TestActivity extends Activity {


    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_layout);

        Button testBt = (Button) findViewById(R.id.testButton);
        testBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedFunctions.setDailyNotification(getBaseContext(), 13, 6);
                //sharedFunctions.createNotification(getBaseContext(), "101 Wants daily notification", "Hello World");
            }
        });
    }

}
