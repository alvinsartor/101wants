package com.ananasproject.hundredwants;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ananasproject.hundredwants.data.WantsContract;

/**
 * A placeholder fragment containing a simple view.
 */
public class InactiveWantsActivityFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    public InactiveWantsActivityFragment() { }

    InactiveWantsCursorAdapter mCursorAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        // Find the ListView which will be populated with the pet data
        ListView wantsListView = (ListView) rootView.findViewById(R.id.list);

        // Find and set empty view on the ListView, so that it only shows when the list has 0 items.
        //View emptyView = rootView.findViewById(R.id.empty_view);
        //wantsListView.setEmptyView(emptyView);


        // Setup an Adapter to create a list item for each row of pet data in the Cursor.
        // There is no pet data yet (until the loader finishes) so pass in null for the Cursor.
        mCursorAdapter = new InactiveWantsCursorAdapter(getContext(), null);
        wantsListView.setAdapter(mCursorAdapter);

        // Kick off the loader
        getLoaderManager().initLoader(1, null, this);

        return rootView;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        // Define a projection that specifies the columns from the table we care about.

        String[] projection = {
                WantsContract.InactiveEntry._ID,
                WantsContract.InactiveEntry.COLUMN_NR,
                WantsContract.InactiveEntry.COLUMN_TEXT,
                WantsContract.InactiveEntry.COLUMN_INSERT_DATE,
                WantsContract.InactiveEntry.COLUMN_COMPLETION_DATE,
                WantsContract.InactiveEntry.COLUMN_CATEGORY
        };

        // This loader will execute the ContentProvider's query method on a background thread
        return new CursorLoader(getContext(),            // Parent activity context
                WantsContract.InactiveEntry.CONTENT_URI,   // Provider content URI to query
                projection,                              // Columns to include in the resulting Cursor
                null,                                    // No selection clause
                null,                                    // No selection arguments
                null);                                   // Default sort order
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // Update {@link PetCursorAdapter} with this new cursor containing updated pet data
        mCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // Callback called when the data needs to be deleted
        mCursorAdapter.swapCursor(null);
    }

}
